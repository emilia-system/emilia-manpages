# Emilia Manpages

AVISO! ESTE PACOTE SERÁ REDISTRIBUIDO LOGO LOGO PARA QUE CADA PACOTE IMPORTE SEUS PRÓPRIOS MANPAGES

Este pacote contém todas as páginas de manual da Emília de todos os programas que foram criados para ela.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

As vezes os programas evoluem rapidamente, não sobrando tempo para atualizar as páginas, favor reportar qualquer inconsistência caso ache.

## Licença

Emilia usa GPL3 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[GPL3](https://choosealicense.com/licenses/gpl-3.0/)
